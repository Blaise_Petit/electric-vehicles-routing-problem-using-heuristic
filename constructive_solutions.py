import matplotlib.pyplot as plt
import numpy as np
import random as rd
import time
from networkx import DiGraph, from_numpy_matrix, relabel_nodes, set_node_attributes
from vrpy import VehicleRoutingProblem
from parse_instance import parse_instance
from evaluating_tools import evaluating_tools


class constructive_solutions:
    """
    This class is bundling methods to generate initial solutions for heuristic.
    """
    def __init__(self, name_file):
        """
        An instance is created to get all the values to compute constructive
        heuristic.

        Returns
        -------
        None.

        """
        self.name_file = name_file
        self.inst = parse_instance(instance_name = name_file)
        self.tools = evaluating_tools(name_file)
        
        self.__dict_coord = self.inst.get_dict_coord()
        self.__dict_demand = self.inst.get_dict_demand()
        self.__dist_array = self.inst.get_dist_array()
        self.__list_cust = self.inst.get_list_cust()
        self.__list_stat = self.inst.get_list_stat()
        self.__Q = self.inst.get_Q()
        self.__C = self.inst.get_C()
        self.__h = self.inst.get_h()
        
        self.coord_cust_x = self.inst.get_coord_cust_x()
        self.coord_cust_y = self.inst.get_coord_cust_y()
        self.coord_stat_x = self.inst.get_coord_stat_x()
        self.coord_stat_y = self.inst.get_coord_stat_y()
        self.dict_coord = self.inst.get_dict_coord()
        self.instance_name = self.inst.get_instance_name()
        
        self.dict_sol = {}
        
    
    def initial_plot(self):
        """
        This function plots the solution in matplotlib fig.

        Parameters
        ----------
        instance_name : str
            Name of the instance.
        coord_cust_x : list
            List of customer x coordinates.
        coord_cust_y : list
            List of customer y coordinates.
        coord_stat_x : list
            List of charging stations x coordinates.
        coord_stat_y : list
            List of charging stations y coordinates.
        dict_coord : dict
            Dictionary of coordinates.

        Returns
        -------
        fig : plt.fig
            Matplotlib fig.
        ax : plt.ax
            Matplotlib ax.

        """
        
        fig, ax = plt.subplots()
        
        ax.set_xlabel("x coordinate")
        ax.set_ylabel("y coordinate")
        ax.set_title(self.instance_name)
        
        cust, = ax.plot(self.coord_cust_x,
                 self.coord_cust_y,
                 "^",
                 label="Customers")
        
        stat, = ax.plot(self.coord_stat_x,
                 self.coord_stat_y,
                 "*",
                 label="Charging Stations")
        
        depot, = ax.plot(self.dict_coord[0][0], self.dict_coord[0][1], "8", label="Depot")
        
        return fig, ax
    
    def update_plot(self, dict_ax_tour, dict_sol, fig, ax, curr_tour_ind, show_labels=True):
        """
        This function is made to update initial plot.

        """
        
        path_x, path_y = [], []
        if "tour_" + str(curr_tour_ind) not in dict_ax_tour:
            dict_ax_tour["tour_"+str(curr_tour_ind)], = ax.plot(path_x, path_y, label = "tour_" + str(curr_tour_ind))
        
        for nodes in dict_sol[curr_tour_ind]:
                
            path_x.append(self.dict_coord[nodes][0])
            path_y.append(self.dict_coord[nodes][1])
                
        dict_ax_tour["tour_"+str(curr_tour_ind)].set_xdata(path_x)
        dict_ax_tour["tour_"+str(curr_tour_ind)].set_ydata(path_y)
            
        fig.canvas.draw()
    
        fig.canvas.flush_events()

        time.sleep(0.1)

        if show_labels:
            ax.legend(bbox_to_anchor=(1.05, 1),
                      loc="best")

        return dict_ax_tour, fig, ax
    
    # Ploting solutions
    def plot_sol(self, sol, show_labels=True, show_index=True):
        """
        Function to plot a solution (non dynamic)

        Parameters
        ----------
        sol : dictionary
            Dictionary of tour representing a solution.
        show_labels : boolean, optional
            DESCRIPTION. The default is True to show labels.
        show_index : boolean, optional
            DESCRIPTION. The default is True to show index of points.

        Returns
        -------
        None.

        """
        
        plt.plot(self.coord_cust_x,
                 self.coord_cust_y,
                 "^",
                 label="Customers")
        
        plt.plot(self.coord_stat_x,
                 self.coord_stat_y,
                 "*",
                 label="Charging Stations")
        
        plt.plot(self.dict_coord[0][0], self.dict_coord[0][1], "8", label="Depot")
        
        if show_index:
            for ind in self.dict_coord:
                plt.annotate(ind,
                             self.dict_coord[ind],
                             textcoords = "offset points",
                             xytext = (0,5),
                             ha = "center")
        
        for ind, tour_ind in enumerate(sol):
            path_x, path_y = [], []
            for nodes in sol[tour_ind]:
                path_x.append(self.dict_coord[nodes][0])
                path_y.append(self.dict_coord[nodes][1])
                
            plt.plot(path_x, path_y, label="Path " + str(ind))
            
        if show_labels:
            plt.legend(bbox_to_anchor=(1.05, 1),
                       loc="upper left",
                       ncol=1,
                       labelspacing=0.05,
                       borderaxespad=0.)
          
        plt.xlabel("x coordinate")
        plt.ylabel("y coordinate")
        plt.title(self.name_file)
        
        plt.show()

    ##### Functions to compute initial solution ####
    
    # Greedy constructive heuristic
    def greedy_solve(self, is_stochastic=False):
        """
        This functions is building a solution with tour starting from depot and
        ending to depot and visiting every customers exactly once.
        For more details about the way it is constructed, please take a look at
        my dissertation.

        Parameters
        ----------
        is_stochastic : boolean, optional
            DESCRIPTION. The default is False for non stochastic construction.

        Returns
        -------
        dict_sol : dictionary
            Dictionary of the constructed solutions.

        """
        
        st = time.time()
        
        # Setting variables of the problem
        dict_coord = self.__dict_coord
        dist_array = self.__dist_array
        dict_demand = self.__dict_demand
        list_cust = self.__list_cust[:]
        list_stat = self.__list_stat[:]
        h = self.__h
        C = self.__C
        Q = self.__Q
        
        # Setting visited and non visited client list
        visited_clients = []
        non_visited_clients = list_cust[:]
        nbr_cust = len(list_cust)
        
        # Setting dictionnary containing solution
        dict_sol = {}
        
        # Setting temporary variables
        curr_tour_ind = 0
        tour = [0]
        
        # Initial plot
        instance_name = self.instance_name
        coord_cust_x = self.coord_cust_x
        coord_cust_y = self.coord_cust_y
        coord_stat_x = self.coord_stat_x
        coord_stat_y = self.coord_stat_y
        dict_coord = self.dict_coord
        
        #fig, ax = self.initial_plot(instance_name, coord_cust_x, coord_cust_y, coord_stat_x, coord_stat_y, dict_coord)
        
        #dict_ax_tour = {}
        
        available_move = True
        
        while len(visited_clients) < nbr_cust and available_move:
            
            # Creates a list of ordered feasible candidates
            list_candidates = self.tools.create_list_feasible_candidates(tour, non_visited_clients)
            ordered_cost_list, dict_candidates = self.tools.cost_candidates(list_candidates)
            
            # If there's at least one remaining candidates
            if len(list_candidates) > 0:
                
                if is_stochastic:
                    rank_cost = rd.randint(0, min(len(list_candidates)-1, 2))
                else:
                    rank_cost = 0
                    
                tour = dict_candidates[ordered_cost_list[rank_cost]]
                #tour = list_candidates[0]
                dict_sol[curr_tour_ind] = tour
                #dict_ax_tour, fig, ax = self.update_plot(dict_ax_tour, dict_sol, fig, ax, curr_tour_ind)
                visited_clients, non_visited_clients = self.tools.update_visited_clients(tour, visited_clients, non_visited_clients)[:]
            
            # Else we terminate the tour and create a new one
            else:
                #print("No candidates")
                if tour == [0]:
                    #print("End while")
                    available_move = False
                    
                else: 
                    #print("New tour")
                    tour = self.tools.ending_tour(tour)[:]
                    dict_sol[curr_tour_ind] = tour
                    #dict_ax_tour, fig, ax = self.update_plot(dict_ax_tour, dict_sol, fig, ax, curr_tour_ind)
                    curr_tour_ind += 1
                    tour = [0]
        
        print(non_visited_clients)
        
        if not available_move:
            for clients in non_visited_clients:
                dict_sol[curr_tour_ind-1] = dict_sol[curr_tour_ind-1][:-1] + [clients] + [0]
        else:  
            dict_sol[curr_tour_ind] = self.tools.ending_tour(dict_sol[curr_tour_ind])[:]
            
        et = time.time()
        elapsed_time = et - st
        print('Execution time solve_naive:', elapsed_time, 'seconds')
        
        return dict_sol
    
    # Initial CVRP solution
    def solve_cvrp(self):
        """
        This function is using vrpy to solve the EVRP as CVRP for initial solutions.

        Returns
        -------
        Optimal cost and optimal solution of the CVRP with exact method.

        """
        
        dist_array_client = self.__dist_array[:len(self.__list_cust)+1 , :len(self.__list_cust)+1]
        
        last_col = []
        
        for el in dist_array_client[:,0]:
            last_col.append([el])
        
        last_col = np.array(last_col, dtype=np.float64)
        last_col.T
        DISTANCES = np.append(dist_array_client, last_col, axis = 1)
        
        last_row = [[0]*(len(DISTANCES)+1)]
        last_row = np.array(last_row)
        DISTANCES = np.append(DISTANCES, last_row, axis = 0)
        DISTANCES[:,0] = 0
        
        # Demands (key: node, value: amount)
        DEMAND = self.__dict_demand
        del DEMAND[0]
        
        # The matrix is transformed into a DiGraph
        A = np.array(DISTANCES, dtype=[("cost", float)])
        G = from_numpy_matrix(A, create_using=DiGraph())
        
        # The demands are stored as node attributes
        set_node_attributes(G, values=DEMAND, name="demand")
        
        
        # The depot is relabeled as Source and Sink
        # The depot is relabeled as Source and Sink
        G = relabel_nodes(G, {0: "Source", len(DISTANCES)-1: "Sink"})
     
        prob = VehicleRoutingProblem(G, load_capacity=int(self.__C))
        temp_min = 1
        prob.solve(time_limit = 60*temp_min)
        #prob.solve(heuristic_only = True)
        #prob.solve()
        
        return prob.best_value, prob.best_tours, prob.best_tours_load
    
    # Initial EVRP
    def initial_evrp(self):
        """
        This function is building a feasible EVRP starting solutions from the
        CVRP solutions obtained with vrpy.

        Returns
        -------
        sol_evrp : dictionary
            Dictionary of tours representing the EVRP first solutions.

        """
        
        sol_cvrp = self.solve_cvrp()[1]
        
        for tour in sol_cvrp:
            for ind, el in enumerate(sol_cvrp[tour]):
                if el == "Source" or el == "Sink":
                    sol_cvrp[tour][ind] = 0
                    
        sol_evrp = self.tools.fixing_cvrp_sol(sol_cvrp.copy())
        
        return sol_evrp