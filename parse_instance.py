import numpy as np
import math as m


class parse_instance:
    """
    This class is build to read every fields of the .evrp instances and define
    all the needed getters.
    """
    def __init__(self, instance_name):
        """
        

        Parameters
        ----------
        instance_name : str
            The name of the evrp file to read as instance of the problem.

        Returns
        -------
        None.

        """
        
        ### Parsing first line to collect instance parameters ###
        
        # Opening the EVRP instance
        self.__instance_name = instance_name
        f = open("evrp-benchmark-set//" + instance_name + ".evrp", "r")
        
        # Creating a dictionary to store instance parameters
        self.__dict_param = dict()
        
        # Reading the parameters of the instance one by one
        self.__dict_param["Name"] = f.readline()[6:-2]
        self.__dict_param["Comment"] = f.readline()[9:-1]
        self.__dict_param["Type"] = f.readline()[6:-2]
        self.__dict_param["Opt_val"] = f.readline()[14:-2]
        self.__dict_param["Nbr_EV"] = int(f.readline()[10:-2])
        self.__dict_param["Dim"] = int(f.readline()[10:-2])
        self.__dict_param["Nbr_stat"] = int(f.readline()[9:-2])
        self.__dict_param["Capacity"] = float(f.readline()[9:-2])
        self.__dict_param["Energy_cap"] = float(f.readline()[16:-2])
        self.__dict_param["Energy_cons"] = float(f.readline()[20:-2])
        self.__dict_param["Edge_weight_format"] = f.readline()[20:-2]
        
        self.__nbr_nodes = self.__dict_param["Dim"] + self.__dict_param["Nbr_stat"]
        
        ### Parsing coordinate of nodes ###
        f.readline()
        self.__dict_coord = {}
        
        for ind in range(0, self.__nbr_nodes):
            l = f.readline().split()
            self.__dict_coord[ind] = (int(l[1]),int(l[2]))
        
        ### Parsing demand of clients ###
        f.readline()
        self.__dict_demand = {}
        self.__list_cust = []
        self.__coord_cust_x = []
        self.__coord_cust_y = []
        
        for ind in range(0, self.__dict_param["Dim"]):
            l = f.readline().split()
            demand = int(l[1])
            if demand > 0:
                self.__list_cust.append(ind)
                self.__coord_cust_x.append(self.__dict_coord[ind][0])
                self.__coord_cust_y.append(self.__dict_coord[ind][1])
            self.__dict_demand[ind] = demand
        
        ### Parsing index of charging stations ###
        f.readline()
        self.__list_stat = []
        self.__coord_stat_x = []
        self.__coord_stat_y = []
        
        for ind in range(self.__dict_param["Nbr_stat"]):
            s = f.readline()
            self.__list_stat.append(int(s)-1)
            self.__coord_stat_x.append(self.__dict_coord[int(s)-1][0])
            self.__coord_stat_y.append(self.__dict_coord[int(s)-1][1])
        
        f.close()
        
        ### Generating useful variables ###

        self.__C = self.__dict_param["Capacity"]
        self.__Q = self.__dict_param["Energy_cap"]
        self.__h = self.__dict_param["Energy_cons"]
        
        self.__dist_array = self.create_distance_matrix(self.__dict_coord)
    
    def euclidian_distance(self, t1, t2):
        """
        A function that takes two tuples representing the 2D coordinates of two nodes
        and returns the euclidian distance as float.

        Parameters
        ----------
        t1 : tuple
            Tuple of dimension two representing the coordinate of the point t1.
        t2 : tuple
            Tuple of dimension two representing the coordinate of the point t2.

        Returns
        -------
        float
            Euclidian distance between the two inputed points.

        """
        
        x1, y1 = t1[0], t1[1]
        x2, y2 = t2[0], t2[1]
        return m.sqrt((x1 - x2)**2 + (y1 - y2)**2)
    
    def create_distance_matrix(self, dict_coord):
        """
        A function that takes the dictionnary of tuples representing 2D  coordinates
        of the EVRP nodes. It returns an array of distances between each nodes.

        Parameters
        ----------
        dict_coord : dict
            Dictionnary with index of nodes as key and tuples of coordinates as value.

        Returns
        -------
        numpy.ndarray
            Multidimensional list of distance, calling [i][j] for the distance between and i and j.

        """
        
        max_ind_nodes = max(dict_coord)+1
        L, l = [], []
        
        for i in range(max_ind_nodes):
            for j in range(max_ind_nodes):
                l.append(self.euclidian_distance(dict_coord[i], dict_coord[j]))
            L.append(l)
            l = []
            
        return np.array(L)   
    
    def get_coord_cust_x(self):
        """
        Just a classic getter.

        Returns
        -------
        list
            List of coordinates of customer on x axis.

        """
        return self.__coord_cust_x
    
    def get_coord_cust_y(self):
        """
        Just a classic getter.

        Returns
        -------
        list
            List of coordinates of customer on y axis.

        """
        return self.__coord_cust_y
    
    def get_coord_stat_x(self):
        """
        Just a classic getter.

        Returns
        -------
        list
            List of coordinates of charging stations on x axis.

        """
        return self.__coord_stat_x
    
    def get_coord_stat_y(self):
        """
        Just a classic getter.

        Returns
        -------
        list
            List of coordinates of charging stations on y axis.

        """
        return self.__coord_stat_y
    
    def get_dict_coord(self):
        """
        Just a classic getter.

        Returns
        -------
        dictionary
            Dictionary of coordinates for each customer nodes.

        """
        return self.__dict_coord
    
    def get_dict_demand(self):
        """
        Just a classic getter.

        Returns
        -------
        dictionary
            Dictionary of the demand for each customer nodes.

        """
        return self.__dict_demand
    
    def get_dist_array(self):
        """
        Just a classic getter.

        Returns
        -------
        np.array
            Array of distance between node i and j.

        """
        return self.__dist_array
    
    def get_instance_name(self):
        """
        Just a classic getter.

        Returns
        -------
        str
            Name of the instance.

        """
        return self.__instance_name
    
    def get_list_cust(self):
        """
        Just a classic getter.

        Returns
        -------
        list
            List of the index of customer.

        """
        return self.__list_cust
    
    def get_list_stat(self):
        """
        Just a classic getter.

        Returns
        -------
        list
            List of the index of charging stations.

        """
        return self.__list_stat
    
    def get_C(self):
        """
        Just a classic getter.

        Returns
        -------
        float
            Value of maximum capacity of vehicles.

        """
        return self.__C
    
    def get_Q(self):
        """
        Just a classic getter.

        Returns
        -------
        float
            Value of the battery capacity of vehicles.

        """
        return self.__Q
    
    def get_h(self):
        """
        Just a classic getter.

        Returns
        -------
        float
            Coefficient of linear battery consumption.

        """
        return self.__h
    
    def get_nbr_ev(self):
        """
        Just a classic getter.

        Returns
        -------
        int
            Number of electrical vehicles in the instance.

        """
        return self.__dict_param["Nbr_EV"]
    
    def get_Dim(self):
        """
        Just a classic getter.

        Returns
        -------
        int
            Dimension (number of customer) of the instance.

        """
        return self.__dict_param["Dim"]
    
    def get_plot_values(self):
        """
        Just a classic getter.

        Returns
        -------
        Every list and str needed to plot the results
        """
        return self.__coord_cust_x, self.__coord_cust_y, self.__coord_stat_x, self.__coord_stat_y, self.__dict_coord, self.__instance_name
    
    def get_instance_values(self):
        """
        Just a classic getter.

        Returns
        -------
        Every list and str needed to instantiate and instance.
        """
        return self.__dict_coord, self.__dict_demand, self.__dist_array, self.__list_cust, self.__list_stat, self.__Q, self.__C, self.__h