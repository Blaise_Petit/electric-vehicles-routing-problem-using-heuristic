### Import libraries ###

from evaluating_tools import evaluating_tools
from constructive_solutions import constructive_solutions
from heuristic_solve import heuristic_solve

### Main ###

# Choosing which instance to solve
name_instance = "E-n22-k4"

# Instantiating the useful classes for this instance
constructive = constructive_solutions(name_instance)
tools = evaluating_tools(name_instance)
heur_solve = heuristic_solve(name_instance)

# Initial solutions can be loaded from pickle file or computed using constructive
# solutions class as shown below :

# Loading initial solution using pickle
sol_cvrp = tools.open_sol_pickle(type_sol = "_cvrp")[0]

# Computing initial solution using
sol_greedy = constructive.greedy_solve()

# Using heuristic method to improve initial solution
sol_heur = heur_solve.heuristic_method(sol_greedy, 5, 0.01)[0]

# Plotting solution
constructive.plot_sol(sol_heur)

# Display solution and cost
print("\nFeasibility status: " + str(tools.sol_ok(sol_heur)) + "\n")
print("Cost of the solution: " + str(tools.cost(sol_heur)))


