import random as rd
import numpy as np
import pandas as pd
import time
import copy
from parse_instance import parse_instance
from evaluating_tools import evaluating_tools

class heuristic_solve:
    
    """
    This class is bundling all the operators as well as heuristic solving algorithm
    itself. 
    The heuristic_method function can be called with any starting solution
    with tours starting with 0, ending with 0 and visiting every clients once, feasible
    or not.
    """
    
    def __init__(self, name_file):
        """
        It creates inst by parsing the instance and retrieve all tools from 
        evaluating_tools instance.

        Parameters
        ----------
        name_file : str
            Name of the instance you want to solve.

        Returns
        -------
        None.

        """
        self.inst = parse_instance(name_file)
        self.tools = evaluating_tools(name_file)
        
        self.nbr_ev = self.inst.get_nbr_ev()
        self.list_stat = self.inst.get_list_stat()
    
    def get_number_heuristic(self):
        """
        Return the number of heuristic operators, sometimes useful for random
        selection of operator.

        Returns
        -------
        int
            The number of heuristic operator.

        """
        return 14
    
    def apply_heuristic(self, h, sol):
        """
        Make the change on the given solution using operator number h.

        Parameters
        ----------
        h : int
            Operator number h is selected.
        sol : dict
            Dictionnary of lists representing the solution.

        Returns
        -------
        dict
            The modified solution according to the chosen operator.

        """
        
        if h == 0:
            return self.swap_visits_inside_tour(sol)
        
        elif h == 1:
            return self. swap_visits_between_tours(sol)
        
        elif h == 2:
            return self.moving_one_visit(sol)
        
        elif h == 3:
            return self.inserting_to_another_tour(sol)
        
        elif h == 4:
            return self.inserting_to_a_new_tour(sol)
        
        elif h == 5:
            return self.adding_station_clever(sol)
        
        elif h == 6:
            return self.adding_station_random(sol)
        
        elif h == 7:
            return self.removing_station(sol)
        
        elif h == 8:
            return self.reversing(sol)
        
        elif h == 9:
            return self.reverse_inserting(sol)
        
        elif h == 10:
            return self.swaping_block(sol)
        
        elif h == 11:
            return self.ruining_and_recreat(sol)
        
        elif h == 12:
            return self.inserting_block(sol)
        
        elif h == 13:
            return self.reverse_swaping(sol)
    
    def swap_visits_inside_tour(self, sol):
        """
        Swap two visits inside one tour.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        if len(rand_tour) > 3:
            x = rd.randint(1, len(rand_tour) - 2)
            y = rd.randint(1, len(rand_tour) - 3)
            
            if x == y:
                y = len(rand_tour) - 2
                
            rand_tour[x], rand_tour[y] = rand_tour[y], rand_tour[x]
            
        return sol
        
    def swap_visits_between_tours(self, sol):
        """
        Swap two visits in two different tours.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        if len(sol) < 2:
            return sol
        
        rand_ind_1 = rd.randint(0, len(sol)-1)
        rand_ind_2 = rd.randint(0, len(sol)-2)
        
        if rand_ind_1 == rand_ind_2:
            rand_ind_2 = len(sol) - 1
            
        rand_tour_1 = sol[rand_ind_1]
        rand_tour_2 = sol[rand_ind_2]
        
        if len(rand_tour_1) >= 3 and len(rand_tour_2) >= 3:
            x = rd.randint(1, len(rand_tour_1) - 2)
            y = rd.randint(1, len(rand_tour_2) - 2)
              
            rand_tour_1[x], rand_tour_2[y] = rand_tour_2[y], rand_tour_1[x]
            
        return sol
    
    def moving_one_visit(self, sol):
        """
        Moves one value visit to the same tour.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        if len(rand_tour) > 3:
            
            loc = rd.randint(1, len(rand_tour) - 2)
            value_loc = rand_tour[loc]
            rand_tour.__delitem__(loc)
            new_loc = rd.randint(1, len(rand_tour) - 2)
            rand_tour.insert(new_loc, value_loc)
            
        return sol
    
    def inserting_to_another_tour(self, sol):
        """
        Moves one visit from one tour to another.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        if len(sol) < 2:
            return sol
        
        rand_ind_1 = rd.randint(0, len(sol)-1)
        rand_ind_2 = rd.randint(0, len(sol)-2)
        
        if rand_ind_1 == rand_ind_2:
            rand_ind_2 = len(sol) - 1
            
        rand_tour_1 = sol[rand_ind_1]
        rand_tour_2 = sol[rand_ind_2]
        
        if len(rand_tour_1) >= 3 and len(rand_tour_2) >= 3:
            
            taking_loc = rd.randint(1, len(rand_tour_1) - 2)
            taking_value = rand_tour_1[taking_loc]
            inserting_loc = rd.randint(1, len(rand_tour_2) - 2)
            
            if len(rand_tour_1) == 3:
                rand_tour_2.insert(inserting_loc, taking_value)
                del sol[rand_ind_1]
                updated_index_sol = {}
                for ind, tour_ind in enumerate(sol):
                    updated_index_sol[ind] = sol[tour_ind]
                return updated_index_sol
            
            else:
                rand_tour_1.remove(taking_value)
                rand_tour_2.insert(inserting_loc, taking_value)
        
        return sol
    
    def inserting_to_a_new_tour(self, sol):
        """
        Move a visit from an existing tour to a new one.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        rand_ind = rd.randint(0, len(sol)-1)
    
        rand_tour = sol[rand_ind]
        taking_loc = rd.randint(1, len(rand_tour) - 2)
        taking_value = rand_tour[taking_loc]
        
        for i in range(self.nbr_ev):
            if len(rand_tour) > 3:
                sol[len(sol)] = [0] + [taking_value] + [0]
                return sol
     
        return sol
        
    def adding_station_clever(self, sol):
        """
        Adding the best charging station in a random tour at a random location.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        loc = rd.randint(1, len(rand_tour) - 2)
        value_before = rand_tour[loc-1]
        value_after = rand_tour[loc]
        # print("loc", loc)
        # print("value_before", value_before)
        # print("value_after", value_after)
        
        cost_insertion_list = []
        
        for stat in self.list_stat:
            cost_insertion_list.append(self.tools.cost_tour([value_before]+[stat]+[value_after]))
            
        chosen_stat = self.list_stat[cost_insertion_list.index(min(cost_insertion_list))]
        # print(chosen_stat)
        
        rand_tour.insert(loc, chosen_stat)
        
        return sol
    
    def adding_station_random(self, sol):
        """
        Adding a random charging station in a random tour at a random position.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        loc = rd.randint(1, len(rand_tour) - 2)
        # print("loc", loc)
        # print("value_before", value_before)
        # print("value_after", value_after)
        
        chosen_stat = rd.choice(self.list_stat)
        # print(chosen_stat)
        
        rand_tour.insert(loc, chosen_stat)
        
        return sol
    
    def removing_station(self, sol):
        """
        Removing a random charging station from a random tour.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """

        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        for i in range(self.nbr_ev):
            stations = []
            
            for ind, el in enumerate(rand_tour):
                if el in self.list_stat:
                    stations.append(el)
                    
            if len(stations) != 0:
                stat_to_remove = rd.choice(stations)
    
                rand_tour.remove(stat_to_remove)
                
                if len(rand_tour) <= 2:
                    del sol[rand_ind]
                    updated_index_sol = {}
                    for ind, tour_ind in enumerate(sol):
                        updated_index_sol[ind] = sol[tour_ind]
                    return updated_index_sol
                
                return sol
        
        return sol
    
    def reversing(self, sol):
        """
        Reversing a random inside part of a tour.
    
        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.
    
        """
        
        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        if len(rand_tour) > 3:
            x = rd.randint(1, len(rand_tour) - 2)
            y = rd.randint(1, len(rand_tour) - 3)
            
            if x == y:
                y = len(rand_tour) - 2
                
            if x < y:
                sol[rand_ind] = rand_tour[:x] + rand_tour[y:x-1:-1] + rand_tour[y+1:]
            else:
                x, y = y, x
                sol[rand_ind] = rand_tour[:x] + rand_tour[y:x-1:-1] + rand_tour[y+1:]
                
        return sol
    
    def reverse_inserting(self, sol):
        """
        This operator is taking a list of visits from one tour, reversing that 
        list and inserting it to another tour.

        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.

        """
        
        if len(sol) < 2:
            return sol
        
        rand_ind_1 = rd.randint(0, len(sol)-1)
        rand_ind_2 = rd.randint(0, len(sol)-2)
        
        if rand_ind_1 == rand_ind_2:
            rand_ind_2 = len(sol) - 1
            
        rand_tour_1 = sol[rand_ind_1]
        rand_tour_2 = sol[rand_ind_2]
        
        if len(rand_tour_1) > 3:
            x = rd.randint(1, len(rand_tour_1) - 2)
            y = rd.randint(1, len(rand_tour_1) - 3)
            
            if x == y:
                y = len(rand_tour_1) - 2
                
            if x < y:
                temp_block = rand_tour_1[y:x-1:-1]
            else:
                x, y = y, x
                temp_block = rand_tour_1[y:x-1:-1]
            #print(temp_block)
        else:
            return sol
        
        if len(rand_tour_1) >= 3:
            loc = rd.randint(1, len(rand_tour_2) - 2)
            #print(loc)
            
            sol[rand_ind_2] = rand_tour_2[:loc] + temp_block + rand_tour_2[loc:]
            sol[rand_ind_1] = rand_tour_1[:x] + rand_tour_1[y+1:]
            
            if len(sol[rand_ind_1]) < 3:
                del sol[rand_ind_1]
                updated_index_sol = {}
                for ind, tour_ind in enumerate(sol):
                    updated_index_sol[ind] = sol[tour_ind]
                return updated_index_sol
                
        return sol
    
    def reverse_swaping(self, sol):
        """
        This operator is taking 2 list of visits from 2 tours, reversing them 
        and inserting each of the reversed list to the other tour.

        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.

        """
        
        if len(sol) < 2:
            return sol
        
        rand_ind_1 = rd.randint(0, len(sol)-1)
        rand_ind_2 = rd.randint(0, len(sol)-2)
        
        if rand_ind_1 == rand_ind_2:
            rand_ind_2 = len(sol) - 1
            
        rand_tour_1 = sol[rand_ind_1]
        rand_tour_2 = sol[rand_ind_2]
        
        if len(rand_tour_1) > 3 and len(rand_tour_2) > 3:
            
            x1 = rd.randint(1, len(rand_tour_1) - 2)
            y1 = rd.randint(1, len(rand_tour_1) - 3)
            
            if x1 == y1:
                y1 = len(rand_tour_1) - 2
                
            x2 = rd.randint(1, len(rand_tour_2) - 2)
            y2 = rd.randint(1, len(rand_tour_2) - 3)
            
            if x2 == y2:
                y2 = len(rand_tour_2) - 2
            
            
            if x1 > y1 and x2 > y2:
                x1, y1 = y1, x1
                x2, y2 = y2, x2
                
                temp_block_1 = rand_tour_1[y1:x1-1:-1]
                temp_block_2 = rand_tour_2[y2:x2-1:-1]
                
                sol[rand_ind_2] = rand_tour_2[:x2] + temp_block_1 + rand_tour_2[y2+1:]
                sol[rand_ind_1] = rand_tour_1[:x1] + temp_block_2 + rand_tour_1[y1+1:]
                
                # print(rand_tour_2[:x2], temp_block_1, rand_tour_2[y2+1:])
                # print(rand_tour_1[:x1], temp_block_2, rand_tour_1[y1+1:])
                
            elif x1 < y1 and x2 < y2:
                temp_block_1 = rand_tour_1[y1:x1-1:-1]
                temp_block_2 = rand_tour_2[y2:x2-1:-1]
                
                sol[rand_ind_2] = rand_tour_2[:x2] + temp_block_1 + rand_tour_2[y2+1:]
                sol[rand_ind_1] = rand_tour_1[:x1] + temp_block_2 + rand_tour_1[y1+1:]
                
                # print(rand_tour_2[:x2], temp_block_1, rand_tour_2[y2+1:])
                # print(rand_tour_1[:x1], temp_block_2, rand_tour_1[y1+1:])
                
            elif x1 < y1 and x2 > y2:
                x2, y2 = y2, x2
                
                temp_block_1 = rand_tour_1[y1:x1-1:-1]
                temp_block_2 = rand_tour_2[y2:x2-1:-1]
                
                sol[rand_ind_2] = rand_tour_2[:x2] + temp_block_1 + rand_tour_2[y2+1:]
                sol[rand_ind_1] = rand_tour_1[:x1] + temp_block_2 + rand_tour_1[y1+1:]
                
                # print(rand_tour_2[:x2], temp_block_1, rand_tour_2[y2+1:])
                # print(rand_tour_1[:x1], temp_block_2, rand_tour_1[y1+1:])
                
            elif x1 > y1 and x2 < y2:
                x1, y1 = y1, x1
                
                temp_block_1 = rand_tour_1[y1:x1-1:-1]
                temp_block_2 = rand_tour_2[y2:x2-1:-1]
                
                sol[rand_ind_2] = rand_tour_2[:x2] + temp_block_1 + rand_tour_2[y2+1:]
                sol[rand_ind_1] = rand_tour_1[:x1] + temp_block_2 + rand_tour_1[y1+1:]
                
                # print(rand_tour_2[:x2], temp_block_1, rand_tour_2[y2+1:])
                # print(rand_tour_1[:x1], temp_block_2, rand_tour_1[y1+1:])
        
        return sol
    
    def swaping_block(self, sol):
        """
        Two list of visits are selected randomly among 2 tours and they are then
        swapped.

        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.

        """
        
        if len(sol) < 2:
            return sol
        
        rand_ind_1 = rd.randint(0, len(sol)-1)
        rand_ind_2 = rd.randint(0, len(sol)-2)
        
        if rand_ind_1 == rand_ind_2:
            rand_ind_2 = len(sol) - 1
            
        #print(rand_ind_1, rand_ind_2)
            
        rand_tour_1 = sol[rand_ind_1]
        rand_tour_2 = sol[rand_ind_2]
        
        if len(rand_tour_1) > 3 and len(rand_tour_2) > 3:
            
            x1 = rd.randint(1, len(rand_tour_1) - 2)
            y1 = rd.randint(1, len(rand_tour_1) - 3)
            
            if x1 == y1:
                y1 = len(rand_tour_1) - 2
                
            x2 = rd.randint(1, len(rand_tour_2) - 2)
            y2 = rd.randint(1, len(rand_tour_2) - 3)
            
            if x2 == y2:
                y2 = len(rand_tour_2) - 2
            
            #print(x1, y1, x2, y2)
            
            if x1 > y1 and x2 > y2:
                x1, y1 = y1, x1
                x2, y2 = y2, x2
                sol[rand_ind_1], sol[rand_ind_2] = rand_tour_1[:x1] + rand_tour_2[x2:y2] + rand_tour_1[y1:], rand_tour_2[:x2] + rand_tour_1[x1:y1] + rand_tour_2[y2:]
                # print(rand_tour_1[:x1], rand_tour_2[x2:y2], rand_tour_1[y1:])
                # print(rand_tour_2[:x2], rand_tour_1[x1:y1], rand_tour_2[y2:])
            elif x1 < y1 and x2 < y2:
                sol[rand_ind_1], sol[rand_ind_2] = rand_tour_1[:x1] + rand_tour_2[x2:y2] + rand_tour_1[y1:], rand_tour_2[:x2] + rand_tour_1[x1:y1] + rand_tour_2[y2:]
                # print(rand_tour_1[:x1], rand_tour_2[x2:y2], rand_tour_1[y1:])
                # print(rand_tour_2[:x2], rand_tour_1[x1:y1], rand_tour_2[y2:])
            elif x1 < y1 and x2 > y2:
                x2, y2 = y2, x2
                sol[rand_ind_1], sol[rand_ind_2] = rand_tour_1[:x1] + rand_tour_2[x2:y2] + rand_tour_1[y1:], rand_tour_2[:x2] + rand_tour_1[x1:y1] + rand_tour_2[y2:]
                # print(rand_tour_1[:x1], rand_tour_2[x2:y2], rand_tour_1[y1:])
                # print(rand_tour_2[:x2], rand_tour_1[x1:y1], rand_tour_2[y2:])
            elif x1 > y1 and x2 < y2:
                x1, y1 = y1, x1
                sol[rand_ind_1], sol[rand_ind_2] = rand_tour_1[:x1] + rand_tour_2[x2:y2] + rand_tour_1[y1:], rand_tour_2[:x2] + rand_tour_1[x1:y1] + rand_tour_2[y2:]
                # print(rand_tour_1[:x1], rand_tour_2[x2:y2], rand_tour_1[y1:])
                # print(rand_tour_2[:x2], rand_tour_1[x1:y1], rand_tour_2[y2:])
        
        return sol
    
    def inserting_block(self, sol):
        """
        A list of visits is randomly selected from one tour and is then inserted
        in a random position of another tour.

        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.

        """
    
        if len(sol) < 2:
            return sol
        
        rand_ind_1 = rd.randint(0, len(sol)-1)
        rand_ind_2 = rd.randint(0, len(sol)-2)
        
        if rand_ind_1 == rand_ind_2:
            rand_ind_2 = len(sol) - 1
            
        #print(rand_ind_1, rand_ind_2)
            
        rand_tour_1 = sol[rand_ind_1]
        rand_tour_2 = sol[rand_ind_2]
        
        if len(rand_tour_1) > 3 and len(rand_tour_2) > 3:
            
            x1 = rd.randint(1, len(rand_tour_1) - 2)
            y1 = rd.randint(1, len(rand_tour_1) - 3)
            
            if x1 == y1:
                y1 = len(rand_tour_1) - 2
            
            if x1 < y1:
                temp_block_1 = rand_tour_1[x1:y1]
                loc = rd.randint(1, len(rand_tour_2) - 2)
                sol[rand_ind_2] = rand_tour_2[:loc] + temp_block_1 + rand_tour_2[loc:]
                sol[rand_ind_1] = rand_tour_1[:x1] + rand_tour_1[y1:]
                
                # print(rand_tour_2[:loc], temp_block_1, rand_tour_2[loc:])
                # print(rand_tour_1[:x1], rand_tour_1[y1:])
                if len(sol[rand_ind_1]) < 3:
        
                    del sol[rand_ind_1]
                    updated_index_sol = {}
                    for ind, tour_ind in enumerate(sol):
                        updated_index_sol[ind] = sol[tour_ind]
                    return updated_index_sol
            else:
                x1, y1 = y1, x1
                
                temp_block_1 = rand_tour_1[x1:y1]
                loc = rd.randint(1, len(rand_tour_2) - 2)
                sol[rand_ind_2] = rand_tour_2[:loc] + temp_block_1 + rand_tour_2[loc:]
                sol[rand_ind_1] = rand_tour_1[:x1] + rand_tour_1[y1:]
                
                # print(rand_tour_2[:loc], temp_block_1, rand_tour_2[loc:])
                # print(rand_tour_1[:x1], rand_tour_1[y1:])
                
                if len(sol[rand_ind_1]) < 3:
        
                    del sol[rand_ind_1]
                    updated_index_sol = {}
                    for ind, tour_ind in enumerate(sol):
                        updated_index_sol[ind] = sol[tour_ind]
                    return updated_index_sol
            
            #print(x1, y1, x2, y2)
            
        
        return sol
    
    
    def ruining_and_recreat(self, sol):
        """
        A random tour is totally ruined and shuffled to be recreate, the only pattern
        that is kept is depot at the begining and at the end of the tour.

        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
    
        Returns
        -------
        sol : dict
            Dictionnary of lists representing the solution.

        """
        
        rand_ind = rd.randint(0, len(sol)-1)
        rand_tour = sol[rand_ind]
        
        if len(rand_tour) <= 3:
            return sol
        else:
            arr = np.array(rand_tour[1:-1])
            np.random.shuffle(arr)
            sol[rand_ind] = [0] + arr.tolist() + [0]
        
        return sol

    
    def heuristic_method(self, sol, threshold_replay, percentage_best_cost):
        """
        This function is trying to improve the given starting solution using the heuristic
        described in the dissertation.

        Parameters
        ----------
        sol : dict
            Dictionnary of lists representing the solution.
        threshold_replay : int
            Integer usually between 1 and 10 that define the rarity of two operators
            in a row.
        percentage_best_cost : float
            Percentage of acceptance range around the best value to accept worsening
            moves.

        Returns
        -------
        best_sol : dict
            Dictionnary of lists representing the best solution found.
        df_uti : pd.DataFrame
            DataFrame of utility score for every operators among the run.
        cost_over_iterations : np.array
            Array of cost at specific iteratios matching iterations_x_axis array.
        elapsed_time : float
            Wall time of the function.

        """
        
        # Array containing iterations index to display cost vs iterations
        iterations_x_axis_1 = np.linspace(0, 10000, 11)
        iterations_x_axis_2 = np.linspace(10000, 100000, 10)
        iterations_x_axis = np.array(list(iterations_x_axis_1[:-1]) + list(iterations_x_axis_2))
        
        # Array containing costs to display cost vs iterations
        cost_over_iterations = np.array([])
        already_stored_index = []
        
        ### Variables initialisation ###
        
        # Parameters initialisation
        max_iter = 100000
        nbr_heuristic = self.get_number_heuristic()
        
        # Current cost and sol are set
        cost = self.tools.cost(sol) + 100000 * self.tools.feasibility(sol)
        new_sol = copy.deepcopy(sol)
        
        # Best cost and sol are set as current sol
        best_sol = copy.deepcopy(sol)
        best_cost = cost
        
        # Utility score is set to 0 for each operators at
        uti = [0] * nbr_heuristic
        llh = list(range(0, nbr_heuristic))
        
        # Index initialisation
        i = 0
        ind_last_best = 0
        
        # Start of the clock for the wall time
        st = time.time()
        
        ### Looping until max iterations ###
        
        while i <= 100000:   
            
            if i - ind_last_best >= 0.05 * max_iter:   # Reseting the utility scores when algorithm is stuck
                  llh = list(range(0, nbr_heuristic))
            
            if i%1000 == 0:                            # Printing every 1000 iterations
                print(i,best_cost)
                
            if i in iterations_x_axis and i not in already_stored_index:   # Storing current cost for plot
                already_stored_index.append(i)
                #print("stock",i,cost)
                cost_over_iterations = np.append(cost_over_iterations, cost)
                
            h = llh[rd.randint(0, len(llh)-1)]        # Chose randomly the operator number h
            sol = self.apply_heuristic(h, sol)        # Applying the operator on the current sol
            
            if rd.randint(0, threshold_replay) == 0:  # There is a chance that we skip evaluation to operate 2 times or more
                continue
            
            cost_new = self.tools.cost(sol) + 100000 * self.tools.feasibility(sol)
            i += 1
            
            if cost_new < best_cost:                  # Best solutions and cost are updated
                ind_last_best = i
                llh.append(h)
                uti[h] += 1
                best_cost = cost_new
                best_sol = copy.deepcopy(sol)
                
            if cost_new <= cost or cost_new <= best_cost + percentage_best_cost*best_cost:    # Only current solution is updated
                cost = cost_new
                new_sol = copy.deepcopy(sol)
                
            else:                                         # The same sol is kept
                sol = copy.deepcopy(new_sol)
        
        et = time.time()                   # End of the clock for wall time
        elapsed_time = et - st             # Computation of wall time
        
        list_operators = ["swap_visits_inside_tour", 
                          "swap_visits_between_tours",
                          "moving_one_visit",
                          "inserting_to_another_tour",
                          "inserting_to_a_new_tour",
                          "adding_station_clever",
                          "adding_station_random",
                          "removing_station",
                          "reversing",
                          "reverse_inserting",
                          "swaping_block",
                          "ruining_and_recreat",
                          "inserting_block",
                          "reverse_swaping"]
        
        df_uti = pd.DataFrame({"Name operator": list_operators, "Utility value": uti})
        
        return best_sol, df_uti, cost_over_iterations, elapsed_time
