import pickle
import numpy as np
from parse_instance import parse_instance


class evaluating_tools:
    """
    This class is bundling every needed tools to evaluate solution of the instance.
    """
    def __init__(self, name_file):
        """
        An instance of the problem is created to allow evaluation using coordinates etc...
        Then all the parameters are stored for all the rest of the methods.

        Parameters
        ----------
        instance_name : str
            The name of the evrp file to read as instance of the problem.

        Returns
        -------
        None.

        """
        
        self.name_file = name_file
        self.inst = parse_instance(instance_name = name_file)
        
        self.__dict_coord = self.inst.get_dict_coord()
        self.__dict_demand = self.inst.get_dict_demand()
        self.__dist_array = self.inst.get_dist_array()
        self.__list_cust = self.inst.get_list_cust()
        self.__list_stat = self.inst.get_list_stat()
        self.__Q = self.inst.get_Q()
        self.__C = self.inst.get_C()
        self.__h = self.inst.get_h()
        
        self.coord_cust_x = self.inst.get_coord_cust_x()
        self.coord_cust_y = self.inst.get_coord_cust_y()
        self.coord_stat_x = self.inst.get_coord_stat_x()
        self.coord_stat_y = self.inst.get_coord_stat_y()
        self.dict_coord = self.inst.get_dict_coord()
        self.instance_name = self.inst.get_instance_name()
        
    ##### Functions to check constraints violations #####
    
    ### Tour capacity ###
        
    def is_tour_capacity_ok(self, tour):
        """
        This function returns True if the capacity of the tour isn't violated

        Parameters
        ----------
        tour : list
            List representing the tour nodes by nodes (ordered).

        Returns
        -------
        bool
            True if the capacity is not violated, False otherwise.

        """
        
        ui = self.__C
        
        for ind in tour:
            list_cust = self.__list_cust[:]
            dict_demand = self.__dict_demand
            
            if ind in list_cust and ind != 0:
                ui -= dict_demand[ind]
        
            if ui < 0:
                return False
            
        return True
    
    def capacity_violation(self, tour):
        """
        The function returns 0 if no capacity violations, the amount of 
        violation otherwise.

        Parameters
        ----------
        tour : list
            List representing the tour nodes by nodes (ordered).

        Returns
        -------
        float
            0 if no violations, the amount of violation otherwise.

        """
        
        ui = self.__C
        
        for ind in tour:
            list_cust = self.__list_cust[:]
            dict_demand = self.__dict_demand
            
            if ind in list_cust and ind != 0:
                ui -= dict_demand[ind]
            
        if ui < 0:
            return ui
        else:
            return 0
            
    ### Tour battery ###
    
    def is_tour_battery_ok(self, tour):
        """
        This function returns True if the battery capacity of the tour 
        isn't violated.

        Parameters
        ----------
        tour : list
            List representing the tour nodes by nodes (ordered).

        Returns
        -------
        bool
            True if the battery capacity is not violated, False otherwise.

        """
        
        yi = self.__Q
        
        for ind in range(1, len(tour)):
            i = tour[ind - 1]
            j = tour[ind]
            Q = self.__Q
            h = self.__h
            dist_array = self.__dist_array
            list_stat = self.__list_stat[:]
            
            yi -= h*dist_array[i][j]
            
            if yi < 0:
                return False
            
            if j in list_stat or j == 0:
                yi = Q
        
        return True
    
    def battery_violation(self, tour):
        """
        The function returns 0 if no battery capacity violations, the amount of 
        violation otherwise.

        Parameters
        ----------
        tour : list
            List representing the tour nodes by nodes (ordered).

        Returns
        -------
        float
            0 if no violations, the amount of battery violation otherwise.

        """
        
        yi = self.__Q
        
        list_remaining_battery = []
        
        for ind in range(1, len(tour)):
            i = tour[ind - 1]
            j = tour[ind]
            Q = self.__Q
            h = self.__h
            dist_array = self.__dist_array
            list_stat = self.__list_stat[:]
            yi -= h*dist_array[i][j]

            list_remaining_battery.append(yi)
            
            if j in list_stat:
                yi = Q
                
        min_battery = min(list_remaining_battery)
        
        if min_battery >= 0:
            return 0
        else:
            return min_battery
    
    def is_tour_not_empty(self, tour):
        """
        This function returns True if not empty, False otherwise.

        Parameters
        ----------
        tour : list
            List representing the tour nodes by nodes (ordered).

        Returns
        -------
        bool
            True if not empty, False otherwise.

        """
        if tour == []:
            return False
        else:
            return True
        
    def cost_candidates(self, list_candidates):
        """
        Considering that a candidate is a tour with a new visits, this function
        is returning the cost (distance) of every candidates by order.

        Parameters
        ----------
        list_candidates : list of list
            List of tour as described above.

        Returns
        -------
        cost_list : list
            Ordered list of cost.
        dict_candidates : dictionary
            dictionary of candidates having cost as key.

        """
        cost_list = [0]*len(list_candidates)
        dict_candidates = {}
        
        for ind, candidates in enumerate(list_candidates):
            cost = self.cost_tour(candidates)
            cost_list[ind] = cost
            dict_candidates[cost] = candidates
            
        return sorted(cost_list), dict_candidates
        
    def ending_tour(self, tour):
        """
        This function take a tour and finish it by going back to depot as well as
        guaranting the feasibility.

        Parameters
        ----------
        tour : list
            List representing the tour nodes by nodes (ordered).

        Returns
        -------
        tour : list
            List representing the tour nodes by nodes (ordered).

        """
        if self.is_tour_battery_ok(tour + [0]):
            return tour + [0]
        else:
            tour = tour + [0]
            while not self.is_tour_battery_ok(tour):
                tour = self.insert_stat_in_best_position(tour)[:]
        return tour
    
    def update_visited_clients(self, tour, visited_clients, non_visited_clients):
        """
        This function is updated the list of visited clients according to a given
        tour that will be added to the solution.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.
        visited_clients : list
            List of the index of the visited clients.
        non_visited_clients : list
            List of the index of the non visited clients.

        Returns
        -------
        visited_clients : list
            Updated list of the index of the visited clients.
        non_visited_clients : list
            Updated list of the index of the non visited clients.

        """
        
        visited_clients = visited_clients[:]
        non_visited_clients = non_visited_clients[:]
        list_cust = self.__list_cust[:]
        
        for node in tour:
            if node not in visited_clients and node in list_cust:
                visited_clients.append(node)
                non_visited_clients.remove(node)
                
        return visited_clients[:], non_visited_clients[:]
    
    def create_list_feasible_candidates(self, tour, non_visited_clients):
        """
        This function is creating a list of all the feasible possbible candidates,
        starting from a tour and a list of possible non visited clients.
        A candidates is a tour containing a new visit compared to the given tour.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.
        non_visited_clients : list
            List of the index of the non visited clients.

        Returns
        -------
        list_candidates : list of list
            List of the different candidates (tour) remaining after feasibility
            filters.

        """
        
        list_candidates = []
        
        for client in non_visited_clients:
            list_candidates.append(tour + [client])
            
        list_candidates = self.remove_capacity_not_ok(list_candidates)[:]
        
        if len(list_candidates) <= 0:
            return []
        
        list_candidates = self.fix_battery_among_candidates(list_candidates)[:]
        if len(list_candidates) <= 0:
            return []
        
        list_candidates = self.remove_empty_tours(list_candidates)[:]
        #print(list_candidates)
        if len(list_candidates) <= 0:
            return []
        
        #print(str(list_candidates) + "\n")
        
        list_candidates = self.remove_far_from_stat(list_candidates)[:]
        #print(list_candidates)
        if len(list_candidates) <= 0:
            return []

        return list_candidates        
        
    def create_list_candidates(self, tour, non_visited_clients):
        """
        This function is creating every possible candidates from tour and the
        non visited clients.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.
        non_visited_clients : list
            List of the index of the non visited clients.

        Returns
        -------
        list_candidates : list of list
            List of all the possible candidates.

        """
        
        list_candidates = []
        
        for ind in range(1, len(tour)):
            for node_client in non_visited_clients:
                list_candidates.append(tour[:ind] + [node_client] + tour[ind:])
        
        for node_client in non_visited_clients:
            list_candidates.append(tour + [node_client])
        
        return list_candidates
            
    
    def cost_insertion(self, vi, vnew, vj):
        """
        This function computes the cost in euclidian distance of inserting a
        point vnew between vi and vj, or the cost of inserting vnew after vi in
        tail of a tour.

        Parameters
        ----------
        vi : int
            Index of the starting point.
        vnew : int
            Index of the inserted point.
        vj : int or str
            Index of last point or "Tail" for insertion in tail of tour.

        Returns
        -------
        float
            The cost of the ask insertion (in euclidian distance).

        """
        dist_array = self.__dist_array
        
        if vj == "Tail":
            return dist_array[vi][vnew]
        else:
            return dist_array[vi][vnew] + dist_array[vnew][vj] - dist_array[vi][vj]
        
    def remove_capacity_not_ok(self, list_candidates):
        """
        This function takes the list of candidates and return a list of all the
        candidates that are note exceeding the capacity limit.

        Parameters
        ----------
        list_candidates : list
            List of list. Every sub list correspond to a candidate tours with
            an inserted nodes.

        Returns
        -------
        list
            The returned list only keeps candidates where the capacity is not
            exceeded.

        """
        return list(filter(self.is_tour_capacity_ok, list_candidates))
    
    def remove_battery_not_ok(self, list_candidates):
        """
        This function is removing the tour that are violating battery assumption
        from the list of candidates already generated.

        Parameters
        ----------
        list_candidates : list of list
            List of all the possible candidates.

        Returns
        -------
        list_candidates : list of list
            List of candidates respecting battery restrictions.

        """
        return list(filter(self.is_tour_battery_ok, list_candidates))
    
    def is_close_from_stat(self, tour):
        """
        The goal of this function is to test if the given tour allow to reach
        a charging station or the depot next or not.

        Parameters
        ----------
        tour : list
            List representing the order of nodes reached by this tour.

        Returns
        -------
        bool
            True is returned if this tour can possibly reach a charging station
            or the depot after its tail. Otherwise, False is returned.

        """
        list_stat = self.__list_stat[:]
        list_stat_dep = [0] + list_stat
        dist_array = self.__dist_array
        Q = self.__Q
        h = self.__h
        
        yi = Q
        last_node = tour[-1]
        for ind in range(1,len(tour)):
            i = tour[ind-1]
            j = tour[ind]
            yi -= h * dist_array[i][j]
          
        for stat_node in list_stat_dep:
            if yi - h * dist_array[last_node][stat_node] >= 0:
                return True
        
        return False
    
    def remove_far_from_stat(self, list_candidates):
        """
        This function is removing the tour that are violating "far from station"
        assumption from the list of candidates already generated.

        Parameters
        ----------
        list_candidates : list of list
            List of all the possible candidates remaining.

        Returns
        -------
        list_candidates : list of list
            List of candidates respecting "far from station" restriction.

        """
        return list(filter(self.is_close_from_stat, list_candidates))
    
    def fix_tour_battery(self, tour):
        """
        This function takes a tour and try everything possible to fix the battery
        constraints by adding the smartest charging station possible.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.

        Returns
        -------
        tour : list
            List of visits starting and ending by depot. This one is supposed
            to pass battery feasibility test.

        """
        
        is_battery_ok = self.is_tour_battery_ok(tour)
        
        if is_battery_ok:
            return tour
        
        else:
            while not is_battery_ok:
                tour = self.insert_stat_in_best_position(tour)[:]
                is_battery_ok = self.is_tour_battery_ok(tour)
                
            return tour
    
    def fix_battery_among_candidates(self, list_candidates):
        """
        This function is fixing the battery capacity problem of some candidates.
        The candidates that cannot be fixed are replaced by an empty list.

        Parameters
        ----------
        list_candidates : list of list
            List of all the possible candidates remaining.

        Returns
        -------
        list_candidates : list of list
            List of all the possible candidates remaining with fixed battery
            capacity and empty lists.

        """
        return list(map(self.fix_tour_battery, list_candidates))
        
    
    def remove_empty_tours(self, list_candidates):
        """
        This function is removing the empty tours introduced by fix_battery_among_candidates.

        Parameters
        ----------
        list_candidates : list of list
            List of all the possible candidates remaining.

        Returns
        -------
        list_candidates : list of list
            List of all the possible candidates remaining without the empty list
            as candidates.

        """
        return list(filter(self.is_tour_not_empty, list_candidates))
    
    
    def insert_stat_between(self, tour, vi, vj, vj_last=False):
        """
        This function is inserting the best possible charging station between
        node vi and node vj in the given tour.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.
        vi : int
            First customer node index.
        vj : int
            Second customer node index.
        vj_last : boolean, optional
            DESCRIPTION. The default is False if vj isn't at the end of the tour
            and True otherwise (because it is a special case).

        Returns
        -------
        tour : list
            List of visits starting and ending by depot. This one has the best
            charging stations inserted between vi and vj.

        """
        
        list_charging_stat_dep = [0] + self.__list_stat[:]
        list_new_tours = []
        list_cost = []
        
        if vj_last:
            index_vi = tour.index(vi)
            index_vj = len(tour) - 1
        else:
            index_vi = tour.index(vi)
            index_vj = tour.index(vj)
        
        for stat in list_charging_stat_dep:
            temp_tour = tour[:index_vi+1] + [stat] + tour[index_vj:]
            
            if self.is_tour_battery_ok(temp_tour):
                list_new_tours.append(temp_tour)
                list_cost.append(self.cost_tour(temp_tour))

        if list_new_tours == []:
            return "Not feasible"
        
        else:
            return list_new_tours[list_cost.index(min(list_cost))]

    def insert_stat_in_best_position(self, tour):
        """
        This function is inserting the best charging stations in the best position
        of the tour to guarantee minimal cost (distance) as well as guaranting
        feasibility.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.

        Returns
        -------
        tour : list
            List of visits starting and ending by depot with station inserted in
            the best position.

        """
        Q = self.__Q
        h = self.__h
        dist_array = self.__dist_array
        
        is_battery_ok = self.is_tour_battery_ok(tour)
        
        if is_battery_ok:
            return tour
        
        else:
            yi = Q
            
            for ind in range(1, len(tour)):
                i = tour[ind-1]
                j = tour[ind]
                yi -= h * dist_array[i][j]
                
                if yi < 0:
                    break
            
            if j == 0:
                tour = self.insert_stat_between(tour, i, j, vj_last=True)[:]
            else:
                tour = self.insert_stat_between(tour, i, j)[:]
            
            if tour == "Not feasible":
                return []
            else:
                return tour
    
    
    ##### Functions to evaluate the quality of solutions #####
    
    def cost_tour(self, tour):
        """
        This function is computing the cost (distance) of one tour.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.

        Returns
        -------
        cost : float
            Total distance traveled in this tour.

        """
        cost = 0
        dist_array = self.__dist_array
        for ind in range(1, len(tour)):
            i = tour[ind - 1]
            j = tour[ind]
            cost += dist_array[i][j]
        return cost
        
    
    def sol_ok(self, sol):
        """
        This function is checking all the feasibility criterion on the given
        solution.

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance.

        Returns
        -------
        bool
            True if the sol is feasible, False otherwise.

        """
        
        for tour_ind in sol:
            is_battery_ok = self.is_tour_battery_ok(sol[tour_ind])
            is_capacity_ok = self.is_tour_capacity_ok(sol[tour_ind])
            
            if not is_battery_ok:
                print("Battery of tour " + str(tour_ind) + " is not ok")
                return False
            
            elif not is_capacity_ok:
                print("Capacity of tour " + str(tour_ind) + " is not ok")
                return False
        
        if not self.all_clients_served(sol):
            print("Not all clients are served")
            return False
        
        return True
    
    
    def all_clients_served(self, sol):
        """
        This function checks if every client are visited exactly once.

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance.

        Returns
        -------
        boolean
            True if every clients are visited exactly once, False otherwise.

        """
        
        list_cust = self.__list_cust[:]
        nbr_clients = len(list_cust)
        
        visited_clients = []
        
        for tour in sol:
            for node in sol[tour]:
                if node != 0 and node not in visited_clients and node in list_cust:
                    visited_clients.append(node)
        
        return len(visited_clients) == nbr_clients
    
    
    def number_non_served_clients(self, sol):
        """
        This function is computing the number of non served clients in the given
        solution.

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance.

        Returns
        -------
        int
            Number of unserved client, could be 0 if they are all served.

        """
        
        list_cust = self.__list_cust[:]
        nbr_clients = len(list_cust)
        
        visited_clients = []
        
        for tour in sol:
            for node in sol[tour]:
                if node != 0 and node not in visited_clients and node in list_cust:
                    visited_clients.append(node)
        
        return len(visited_clients) - nbr_clients

    
    def spot_tour_with_battery_prob(self, sol):
        """
        This function is returning the list of index of tour having a battery
        violation in the given solution.

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance.
        Returns
        -------
        list_prob : list
            List of index of tour having a battery violation in the given solution.

        """
        
        list_prob = []
        
        for ind_tour in sol:
            if not self.is_tour_battery_ok(sol[ind_tour]):
                list_prob.append(ind_tour)
                
        return list_prob
    
    
    def create_candidates_charging_station(self, tour):
        """
        This function is creating every candidates of charging station insertion
        in one tour.

        Parameters
        ----------
        tour : list
            List of visits starting and ending by depot.

        Returns
        -------
        list_candidates_bat : list of list
            List of the candidates of chargins station insertion.

        """
        list_candidates_bat = []
        
        for ind in range(1, len(tour)):
            for stat in self.__list_stat:
                list_candidates_bat.append(tour[:ind] + [stat] + tour[ind:])
                
        return list_candidates_bat
    
    
    def adding_mirrored_candidates(self, list_candidates_bat):
        """
        Adding all the fully reversed candidates to the list of candidates.

        Parameters
        ----------
        list_candidates_bat : list of list
            List of the candidates of chargins station insertion.

        Returns
        -------
        list_candidates_bat : list of list
            List of the candidates of chargins station insertion as well as their
            mirrored version.

        """
        list_candidates_bat_full = list_candidates_bat[:]
        
        for candidates in list_candidates_bat:
            list_candidates_bat_full.append(list(reversed(candidates)))
            
        return list_candidates_bat_full
    
    
    def best_battery_candidate(self, list_candidates_bat):
        """
        This function returns the best candidates (minimal cost) among the list
        of candidates.

        Parameters
        ----------
        list_candidates_bat : list of list
            List of the candidates of chargins station insertion.

        Returns
        -------
        list
            Candidates representing the tour with the minimal distance.

        """
        
        list_cost = []
        for candidate in list_candidates_bat:
            list_cost.append(self.cost_tour(candidate))
        
        ind_min_cost = list_cost.index(min(list_cost))
        
        return list_candidates_bat[ind_min_cost]
    
    
    def fixing_cvrp_sol(self, sol):
        """
        This function is taking the solution of CVRP problem and is fixing all
        the battery issues it has as an EVRP problem.

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance considered as CVRP.

        Returns
        -------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance considered as EVRP (battery fixed).

        """
        
        list_prob = self.spot_tour_with_battery_prob(sol)
        
        for ind_tour in list_prob:
            list_candidates_bat = self.create_candidates_charging_station(sol[ind_tour])
            list_candidates_bat = self.adding_mirrored_candidates(list_candidates_bat)
            list_candidates_bat = self.remove_battery_not_ok(list_candidates_bat)
            
            if list_candidates_bat == []:
                return sol
            else:
                sol[ind_tour] = self.best_battery_candidate(list_candidates_bat)
        
        return sol
    
    def cost(self, sol):
        """
        This function is computing the cost of the solution i.e. the total traveled
        distance.

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance.

        Returns
        -------
        S : float
            Cost i.e. total traveled distance by this solution.

        """
        
        S = 0
        dist_array = self.__dist_array
        
        for tour_ind in sol:
            
            s = 0
            
            for ind in range (1, len(sol[tour_ind])):
                i = sol[tour_ind][ind - 1]
                j = sol[tour_ind][ind]
                
                s += dist_array[i][j]
                
            S += s
            s = 0
            
        return S
    
    def feasibility(self, sol):
        """
        This function gives a score of feasibility according to violation: the
        bigger the score is the worse the feasibility is. A score of 0 means "feasible"

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance considered as CVRP.

        Returns
        -------
        float
            Score of feasibility as described above.

        """
        
        battery_violation = 0
        capacity_violation = 0
        
        for tour_ind in sol:
            battery_violation += self.battery_violation(sol[tour_ind])
            capacity_violation += self.capacity_violation(sol[tour_ind])
         
        unique_client_violation = self.unicity_of_client_violation(sol)
        
        coeff_battery = 1
        coeff_capacity = 1
        coeff_unique_client = 10
        
        return coeff_battery * abs(battery_violation) + coeff_capacity * abs(capacity_violation) + coeff_unique_client * unique_client_violation
    
    def unicity_of_client_violation(self, sol):
        """
        This function computes the unicity of client violation, i.e. 1 if one client
        is visited twice, 2 if two clients are visited twice or one client is
        visited 3 times etc..

        Parameters
        ----------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance considered as CVRP.

        Returns
        -------
        int
            Score of unicity of client violation as described before.

        """
        list_sol = []
        
        for tour_ind in sol:
            list_sol += sol[tour_ind][1:-1]
            
        array_sol = np.array(list_sol)
        array_client = array_sol[array_sol <= max(self.__list_cust)]
        
        set_client = set(array_client.tolist())
        
        return len(array_client) - len(set_client)
    
    def open_sol_pickle(self, type_sol = "evrp_cvrp"):
        """
        This function is built to de-serialize already computed starting solutions
        from .pickle files.

        Parameters
        ----------
        type_sol : str, optional
            DESCRIPTION. The default is "evrp_cvrp" but "greedy" also exist.

        Returns
        -------
        sol : dictionary
            Dictionary of every chosen tours representing a solution to the
            instance considered as CVRP.
        cost : float
            Corresponding cost to the read solution.

        """
        # Set sol, set instance values
        
        complete_path = "Pickle solutions" + "//" + self.name_file + type_sol + ".pickle"
        pickle_in = open(complete_path, "rb")
        dict_pickle = pickle.load(pickle_in)
        
        sol = dict_pickle["sol"]
        cost = dict_pickle["cost"]
        
        return sol, cost